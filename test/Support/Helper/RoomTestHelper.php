<?php

declare(strict_types=1);

namespace Jh3ady\RealWorldPlayground\Test\Support\Helper;

use Jh3ady\RealWorldPlayground\Building\Coordinate;
use Jh3ady\RealWorldPlayground\Building\Room;
use Jh3ady\RealWorldPlayground\Building\StructureInterface;

final class RoomTestHelper
{
    /**
     * @var Room
     */
    private Room $room;

    private function __construct()
    {
        $this->room = new Room();
    }

    /**
     * @return static
     */
    public static function create(): self
    {
        return new self();
    }

    /**
     * @param StructureInterface $structure
     * @return self
     */
    public function addStructure(StructureInterface $structure): self
    {
        $this->room->addStructure($structure);

        return $this;
    }

    /**
     * @return Room
     */
    public function getRoom(): Room
    {
        return $this->room;
    }
}