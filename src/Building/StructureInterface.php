<?php

declare(strict_types=1);

namespace Jh3ady\RealWorldPlayground\Building;

interface StructureInterface
{
    /**
     * @param StructureInterface $other
     * @return bool
     */
    public function isOverlapping(StructureInterface $other): bool;
}