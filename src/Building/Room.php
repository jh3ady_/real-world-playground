<?php

declare(strict_types=1);

namespace Jh3ady\RealWorldPlayground\Building;

use InvalidArgumentException;

class Room implements AreaInterface, RoomInterface
{
    /**
     * @var StructureInterface[]
     */
    protected array $structures = [];

    /**
     * @param StructureInterface $structure
     * @return void
     */
    public function addStructure(StructureInterface $structure): void
    {
        foreach ($this->structures as $existingStructure) {
            if ($structure->isOverlapping($existingStructure)) {
                throw new InvalidArgumentException('Structure overlaps with existing structure');
            }
        }

        $this->structures[] = $structure;
    }

    /**
     * @return StructureInterface[]
     */
    public function getStructures(): array
    {
        return $this->structures;
    }
}
