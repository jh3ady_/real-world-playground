<?php


namespace Jh3ady\RealWorldPlayground\Test\Unit\Building;

use Codeception\Test\Unit;
use Jh3ady\RealWorldPlayground\Building\Coordinate;
use Jh3ady\RealWorldPlayground\Test\Support\Helper\WallTestHelper;
use Jh3ady\RealWorldPlayground\Test\Support\UnitTester;

class WallTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected UnitTester $tester;

    /**
     * @return void
     */
    protected function _before(): void
    {
    }

    // tests
    public function testHaveCoordinates(): void
    {
        $wall = WallTestHelper::create()
            ->setCoordinates(
                new Coordinate(0, 0),
                new Coordinate(0, 4)
            )
            ->getWall();

        $this->assertEquals(new Coordinate(0, 0), $wall->getStartCoordinate());
        $this->assertEquals(new Coordinate(0, 4), $wall->getEndCoordinate());
    }
}
