<?php


namespace Jh3ady\RealWorldPlayground\Test\Unit\Building;

use Codeception\Test\Unit;
use Jh3ady\RealWorldPlayground\Building\Coordinate;
use Jh3ady\RealWorldPlayground\Test\Support\UnitTester;

final class CoordinateTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected UnitTester $tester;

    protected function _before(): void
    {
    }

    /**
     * @return void
     */
    public function testGetX(): void
    {
        $coordinate = new Coordinate(0, 1);
        $this->assertEquals(0, $coordinate->getX());
    }

    /**
     * @return void
     */
    public function testGetY(): void
    {
        $coordinate = new Coordinate(1, 0);
        $this->assertEquals(0, $coordinate->getY());
    }

}
