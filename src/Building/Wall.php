<?php

declare(strict_types=1);

namespace Jh3ady\RealWorldPlayground\Building;

class Wall implements StructureInterface
{
    /**
     * @var Coordinate
     */
    protected Coordinate $startCoordinate;

    /**
     * @var Coordinate
     */
    protected Coordinate $endCoordinate;

    public function __construct()
    {
    }

    /**
     * @param Coordinate $start
     * @param Coordinate $end
     * @return void
     */
    public function setCoordinates(Coordinate $start, Coordinate $end): void
    {
        $this->startCoordinate = $start;
        $this->endCoordinate = $end;
    }

    /**
     * @return Coordinate
     */
    public function getStartCoordinate(): Coordinate
    {
        return $this->startCoordinate;
    }

    /**
     * @return Coordinate
     */
    public function getEndCoordinate(): Coordinate
    {
        return $this->endCoordinate;
    }

    /**
     * @param StructureInterface $other
     * @return bool
     */
    public function isOverlapping(StructureInterface $other): bool
    {
        if ($this->startCoordinate->equals($other->getStartCoordinate()) && $this->endCoordinate->equals($other->getEndCoordinate())) {
            return true;
        }

        if ($this->startCoordinate->equals($other->getEndCoordinate()) && $this->endCoordinate->equals($other->getStartCoordinate())) {
            return true;
        }

        if ($this->startCoordinate->equals($other->getStartCoordinate()) && $this->endCoordinate->isBetween($other->getStartCoordinate(), $other->getEndCoordinate())) {
            return true;
        }

        if ($this->startCoordinate->equals($other->getEndCoordinate()) && $this->endCoordinate->isBetween($other->getStartCoordinate(), $other->getEndCoordinate())) {
            return true;
        }

        if ($this->endCoordinate->equals($other->getStartCoordinate()) && $this->startCoordinate->isBetween($other->getStartCoordinate(), $other->getEndCoordinate())) {
            return true;
        }

        if ($this->endCoordinate->equals($other->getEndCoordinate()) && $this->startCoordinate->isBetween($other->getStartCoordinate(), $other->getEndCoordinate())) {
            return true;
        }

        if ($this->startCoordinate->isBetween($other->getStartCoordinate(), $other->getEndCoordinate()) && $this->endCoordinate->isBetween($other->getStartCoordinate(), $other->getEndCoordinate())) {
            return true;
        }

        if ($other->getStartCoordinate()->isBetween($this->startCoordinate, $this->endCoordinate) && $other->getEndCoordinate()->isBetween($this->startCoordinate, $this->endCoordinate)) {
            return true;
        }

        return false;
    }

//    /**
//     * @param StructureInterface $other
//     * @return bool
//     */
//    public function isOverlapping(StructureInterface $other): bool
//    {
//        $otherStart = $other->getStartCoordinate();
//        $otherEnd = $other->getEndCoordinate();
//
//        $start = $this->getStartCoordinate();
//        $end = $this->getEndCoordinate();
//
//        if ($otherStart->getX() >= $start->getX() && $otherStart->getX() <= $end->getX()) {
//            if ($otherStart->getY() >= $start->getY() && $otherStart->getY() <= $end->getY()) {
//                return true;
//            }
//        }
//
//        if ($otherEnd->getX() >= $start->getX() && $otherEnd->getX() <= $end->getX()) {
//            if ($otherEnd->getY() >= $start->getY() && $otherEnd->getY() <= $end->getY()) {
//                return true;
//            }
//        }
//
//        return false;
//    }
}