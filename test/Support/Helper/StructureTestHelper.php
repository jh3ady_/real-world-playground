<?php

declare(strict_types=1);

namespace Jh3ady\RealWorldPlayground\Test\Support\Helper;

use Jh3ady\RealWorldPlayground\Building\Coordinate;
use Jh3ady\RealWorldPlayground\Building\StructureInterface;

final class StructureTestHelper
{
    /**
     * @var Coordinate
     */
    private Coordinate $startCoordinate;

    /**
     * @var Coordinate
     */
    private Coordinate $endCoordinate;

    /**
     * @param StructureInterface $structure
     */
    private function __construct(private StructureInterface $structure)
    {
    }

    /**
     * @param string $structureClassName
     * @return static
     */
    public static function create(string $structureClassName): self
    {
        return new self(new $structureClassName());
    }

    /**
     * @param int $x
     * @param int $y
     * @return $this
     */
    public function setStartCoordinate(int $x, int $y): self
    {
        $this->startCoordinate = new Coordinate($x, $y);

        return $this;
    }

    /**
     * @param int $x
     * @param int $y
     * @return $this
     */
    public function setEndCoordinate(int $x, int $y): self
    {
        $this->endCoordinate = new Coordinate($x, $y);

        return $this;
    }

    /**
     * @return StructureInterface
     */
    public function getStructure(): StructureInterface
    {
        $this->structure->setCoordinates($this->startCoordinate, $this->endCoordinate);

        return $this->structure;
    }
}