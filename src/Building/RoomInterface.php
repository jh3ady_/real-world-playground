<?php

declare(strict_types=1);

namespace Jh3ady\RealWorldPlayground\Building;

interface RoomInterface
{
    /**
     * @param StructureInterface $structure
     * @return void
     */
    public function addStructure(StructureInterface $structure): void;

    /**
     * @return StructureInterface[]
     */
    public function getStructures(): array;
}