<?php

declare(strict_types=1);

namespace Jh3ady\RealWorldPlayground\Building;

final class Coordinate
{
    /**
     * @param int $x
     * @param int $y
     */
    public function __construct(private readonly int $x, private readonly int $y)
    {
    }

    /**
     * @return int
     */
    public function getX(): int
    {
        return $this->x;
    }

    /**
     * @return int
     */
    public function getY(): int
    {
        return $this->y;
    }

    /**
     * @param Coordinate $other
     * @return bool
     */
    public function equals(Coordinate $other): bool
    {
        return $this->x === $other->getX() && $this->y === $other->getY();
    }

    /**
     * @param Coordinate $getStartCoordinate
     * @param Coordinate $getEndCoordinate
     * @return bool
     */
    public function isBetween(Coordinate $getStartCoordinate, Coordinate $getEndCoordinate): bool
    {
        return $this->x >= $getStartCoordinate->getX() && $this->x <= $getEndCoordinate->getX() && $this->y >= $getStartCoordinate->getY() && $this->y <= $getEndCoordinate->getY();
    }
}
