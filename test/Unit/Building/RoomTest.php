<?php


namespace Jh3ady\RealWorldPlayground\Test\Unit\Building;

use Codeception\Test\Unit;
use InvalidArgumentException;
use Jh3ady\RealWorldPlayground\Building\Wall;
use Jh3ady\RealWorldPlayground\Test\Support\Helper\RoomTestHelper;
use Jh3ady\RealWorldPlayground\Test\Support\Helper\StructureTestHelper;
use Jh3ady\RealWorldPlayground\Test\Support\UnitTester;

final class RoomTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected UnitTester $tester;

    /**
     * @return void
     */
    protected function _before(): void
    {
    }

    /**
     * @return void
     */
    public function testHaveOneWall(): void
    {
        $room = RoomTestHelper::create()
            ->addStructure(
                StructureTestHelper::create(Wall::class)
                    ->setStartCoordinate(0, 0)
                    ->setEndCoordinate(0, 4)
                    ->getStructure()
            )
            ->getRoom();

        $structures = $room->getStructures();
        $this->assertCount(1, $structures);
    }

    /**
     * @return void
     */
    public function testCantHaveAWallOverlappingAnotherWithSameSize(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Structure overlaps with existing structure');

        RoomTestHelper::create()
            ->addStructure(
                StructureTestHelper::create(Wall::class)
                    ->setStartCoordinate(0, 0)
                    ->setEndCoordinate(0, 4)
                    ->getStructure()
            )
            ->addStructure(
                StructureTestHelper::create(Wall::class)
                    ->setStartCoordinate(0, 0)
                    ->setEndCoordinate(0, 4)
                    ->getStructure()
            )
            ->getRoom();
    }

    /**
     * @return void
     */
    public function testCantHaveAWallOverlappingAnotherWithLowerSize(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Structure overlaps with existing structure');

        RoomTestHelper::create()
            ->addStructure(
                StructureTestHelper::create(Wall::class)
                    ->setStartCoordinate(0, 0)
                    ->setEndCoordinate(0, 4)
                    ->getStructure()
            )
            ->addStructure(
                StructureTestHelper::create(Wall::class)
                    ->setStartCoordinate(0, 0)
                    ->setEndCoordinate(0, 3)
                    ->getStructure()
            )
            ->getRoom();
    }

    /**
     * @return void
     */
    public function testCantHaveAWallOverlappingAnotherWithGreaterSize(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Structure overlaps with existing structure');

        RoomTestHelper::create()
            ->addStructure(
                StructureTestHelper::create(Wall::class)
                    ->setStartCoordinate(0, 0)
                    ->setEndCoordinate(0, 4)
                    ->getStructure()
            )
            ->addStructure(
                StructureTestHelper::create(Wall::class)
                    ->setStartCoordinate(0, 0)
                    ->setEndCoordinate(0, 5)
                    ->getStructure()
            )
            ->getRoom();
    }

    /**
     * @return void
     */
    public function testHaveTwoWallsLShape(): void
    {
        $room = RoomTestHelper::create()
            ->addStructure(
                StructureTestHelper::create(Wall::class)
                    ->setStartCoordinate(0, 0)
                    ->setEndCoordinate(4, 0)
                    ->getStructure()
            )
            ->addStructure(
                StructureTestHelper::create(Wall::class)
                    ->setStartCoordinate(0, 0)
                    ->setEndCoordinate(0, 4)
                    ->getStructure()
            )
            ->getRoom();

        $structures = $room->getStructures();
        $this->assertCount(2, $structures);
    }

    /**
     * @return void
     */
    public function testHaveThreeWallsUShape(): void
    {
        $room = RoomTestHelper::create()
            ->addStructure(
                StructureTestHelper::create(Wall::class)
                    ->setStartCoordinate(0, 0)
                    ->setEndCoordinate(4, 0)
                    ->getStructure()
            )
            ->addStructure(
                StructureTestHelper::create(Wall::class)
                    ->setStartCoordinate(0, 0)
                    ->setEndCoordinate(0, 4)
                    ->getStructure()
            )
            ->addStructure(
                StructureTestHelper::create(Wall::class)
                    ->setStartCoordinate(0, 4)
                    ->setEndCoordinate(4, 4)
                    ->getStructure()
            )
            ->getRoom();

        $structures = $room->getStructures();
        $this->assertCount(3, $structures);
    }

    /**
     * @return void
     */
    public function testHaveFourWallsSquareShape(): void
    {
        $room = RoomTestHelper::create()
            ->addStructure(
                StructureTestHelper::create(Wall::class)
                    ->setStartCoordinate(0, 0)
                    ->setEndCoordinate(4, 0)
                    ->getStructure()
            )
            ->addStructure(
                StructureTestHelper::create(Wall::class)
                    ->setStartCoordinate(0, 0)
                    ->setEndCoordinate(0, 4)
                    ->getStructure()
            )
            ->addStructure(
                StructureTestHelper::create(Wall::class)
                    ->setStartCoordinate(0, 4)
                    ->setEndCoordinate(4, 4)
                    ->getStructure()
            )
            ->addStructure(
                StructureTestHelper::create(Wall::class)
                    ->setStartCoordinate(4, 0)
                    ->setEndCoordinate(4, 4)
                    ->getStructure()
            )
            ->getRoom();

        $structures = $room->getStructures();
        $this->assertCount(4, $structures);
    }

    /**
     * @return void
     */
    public function testHaveFourWallsCrossShape(): void
    {
        $room = RoomTestHelper::create()
            ->addStructure(
                StructureTestHelper::create(Wall::class)
                    ->setStartCoordinate(0, 0)
                    ->setEndCoordinate(4, 0)
                    ->getStructure()
            )
            ->addStructure(
                StructureTestHelper::create(Wall::class)
                    ->setStartCoordinate(0, 0)
                    ->setEndCoordinate(0, 4)
                    ->getStructure()
            )
            ->addStructure(
                StructureTestHelper::create(Wall::class)
                    ->setStartCoordinate(0, 0)
                    ->setEndCoordinate(-4, 0)
                    ->getStructure()
            )
            ->addStructure(
                StructureTestHelper::create(Wall::class)
                    ->setStartCoordinate(0, 0)
                    ->setEndCoordinate(0, -4)
                    ->getStructure()
            )
            ->getRoom();

        $structures = $room->getStructures();
        $this->assertCount(4, $structures);
    }
}
